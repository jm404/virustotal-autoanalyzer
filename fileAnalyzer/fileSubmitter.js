﻿
var con = "";
var vt = require("node-virustotal");
var fs = require("fs");
var queueManager = require("./queueManager");
var queueObjects = require("./analyzing/queue.json");

function connectVirusTotal() {
    try {
        con = vt.MakePublicConnection();
        con.setKey("e2513a75f92a4169e8a47b4ab1df757f83ae45008b4a8a49903450c8402add4d"); // Default key
        con.setDelay(15000);                                                            // Default delay stablished by virustotal
    } catch (error) {
        console.error(error);
        console.log("Error in connection with virustotal. Check internet connection.");
    }
    

}

module.exports = {

    submitFile: function submitFile(fileName, mimeType) {

        connectVirusTotal();

        con.submitFileForAnalysis(fileName, mimeType, fs.readFileSync(fileName), function (data) {
            console.log("");
            console.log("File \"" + fileName + "\" sent. Report could take some time depending on virustotal.com load.");
            console.log("Results will be saved on results folder.");
            console.log("Scan id:" + data.scan_id);
            console.log("");
            console.log("Waiting for files to scan........");

            queueManager.addToQueue(data.scan_id.toString());

        }, function (mistake) {
            console.log(mistake);
        });

        
    }, getReports: function getReports() {

        // Obtaining scanID

        var scanID = queueObjects["queue"][0].scanID;
        console.log("Obtaining results of " + scanID + "\n");

        if (con == "") {
                try {
                    connectVirusTotal();
                } catch (error) {
                    console.error(error);
                    console.log("Error when connecting to virustotal.");
                }

            }

        try {

                con.getFileReport(scanID, function (data) {
                    try {
                        console.log(data);
                    } catch (error) {
                        console.error(error);
                        console.log(error);
                    }

                    queueManager.removeFirst();

                }, function (mistake) {
                    console.log(mistake);
                });

            } catch (error) {
                console.log("Timeout received from virustotal to avoid overload. Retrying in 2 minutes...");
            }


    }


}