﻿var mime = require("mime-types");

module.exports = {

    mimeGetter: function mimeGetter(file) {
        var mimeTypeResult = mime.lookup(file);
        return mimeTypeResult;
    }

};