﻿
var fileSubmitter = require("./fileSubmitter");
var cron = require("node-cron");
var queueObjects = require("./analyzing/queue.json");

console.log("Obtaining results of queued files... This could take a few moments depending on virustotal.com load.");

var isQueueEmpty = false;



cron.schedule('* * * * *', function (error) {

    if (Object.keys(queueObjects["queue"]).length > 0) {

        try {
            fileSubmitter.getReports();
        } catch (error) {
            console.error(error);
            console.log("Error when obtaining results");
        }

    } else {
        console.log("Scanning queue is empty. Waiting for files to scan...");
    }
    
});