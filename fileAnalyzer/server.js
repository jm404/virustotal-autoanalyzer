

var events = require("events");
var util = require("util");
var logfile = "./changes.txt";
var fs = require("fs")
var inboxDir = "./inbox";
var analyzingDir = "./analyzing";
var fileSubmitter = require("./fileSubmitter");
var mimeGetter = require("./mimeGetter");

util.inherits(Watcher, events.EventEmitter);

function Watcher(inboxDir, analyzingDir) {
    this.inboxDir = inboxDir;
    this.analyzingDir = analyzingDir;
}
 
Watcher.prototype = new events.EventEmitter();

Watcher.prototype.watch = function () {
    var watcher = this;
    fs.readdir(this.inboxDir, function (err, files) {
        if (err) throw err;
        for (var index in files) {
            watcher.emit("process", files[index]);
        }

    })
}

Watcher.prototype.start = function () {
    var watcher = this;

    try {

        fs.watchFile(inboxDir, function () {
            watcher.watch();
        });

    } catch(error) {
        console.error(error);
        console.log("Error when trying to start watcher, see errors for more details.")
    }
}

var watcher01 = new Watcher(inboxDir, analyzingDir);

watcher01.on("process", function process(file) {

    var watchFile = this.inboxDir + "/" + file;
    var processedFile = this.analyzingDir + "/" + file;
    var fileMimeType = mimeGetter.mimeGetter(watchFile);

    try {
        fileSubmitter.submitFile(watchFile, fileMimeType);
    } catch (error) {
        console.error(error);
        console.log("Error when queueing file.");

    }
    try {
        fs.rename(watchFile, processedFile, function (err) { // Typical way to move files (oldpath,newpath,callback)
            if (err) {
                throw err;
            }
        });
    } catch (error) {
        console.error(error);
        console.log("Error when moving file from inbox.");
    }


});

// Execution
watcher01.start();


