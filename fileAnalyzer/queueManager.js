﻿//check if empty
//call for results
//create logs 
// manage queue

var fileSubmiter = require("./fileSubmitter.js");
var fs = require("fs");
var queueObjects = require("./analyzing/queue.json");

module.exports = {

    addToQueue: function addToQueue(scanID) {

        var itemToAdd = {
            "scanID": scanID,
        };

        queueObjects["queue"].push(itemToAdd);
        var queueString = JSON.stringify(queueObjects);

        fs.writeFile("./analyzing/queue.json", queueString, function (err) {
            if (err) throw err;
        });
    },

    removeFirst: function removeFirst() {

        if ((queueObjects["queue"][1] === undefined)) {
            var queueSubmitt = queueObjects.queue.splice(0, 0);
        } else {
            var queueSubmitt = queueObjects.queue.splice(1, Object.keys(queueObjects["queue"]).length);
        }

        var queueString = "{\"queue\":" + JSON.stringify(queueSubmitt) + "}";

        fs.writeFile("./analyzing/queue.json", queueString, function (err) {
            if (err) throw err;
        });

    }

 };


/*
for (var i = 0; i < queue.length; i++) {
    fileSubmiter.getReport(queue[i].scanID);
}

*/
