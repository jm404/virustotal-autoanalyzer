
function Watcher(inboxDir, analyzingDir) {
    this.inboxDir = inboxDir;
    this.analyzingDir = analyzingDir;
}

var events = require("events");
var util = require("util");
var logfile = "./changes.txt";
var fs = require("fs")
var inboxDir = "./inbox";
var analyzingDir = "./analyzing";
var fileSubmitter = require("./fileSubmitter");
var mimeGetter = require("./mimeGetter");
var cron = require("node-cron");
var queueObjects = ""

util.inherits(Watcher, events.EventEmitter);
Watcher.prototype = new events.EventEmitter();

Watcher.prototype.watch = function () {
    var watcher = this;
    fs.readdir(this.inboxDir, function (err, files) {
        if (err) throw err;
        for (var index in files) {
            watcher.emit("process", files[index]);
        }

    })
}

Watcher.prototype.start = function () {
    var watcher = this;
    fs.watchFile(inboxDir, function () {
        watcher.watch();
    });
}

var watcher01 = new Watcher(inboxDir, analyzingDir);

watcher01.on("process", function process(file) {

    var watchFile = this.inboxDir + "/" + file;
    var processedFile = this.analyzingDir + "/" + file;

    var fileMimeType = mimeGetter.mimeGetter(watchFile);
    fileSubmitter.submitFile(watchFile, fileMimeType);

    fs.rename(watchFile, processedFile, function (err) { // Typical way to move files (oldpath,newpath,callback)
        if (err) {
            throw err;
        }
    });

});

// Execution
watcher01.start();


